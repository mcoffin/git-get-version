# git-get-version

`git-get-version` is a git command for easily gleaning version information from a git repository, and formatting it.

# Usage

```
git get-version [-a] [-v]
```

## Options

| Option | Purpose |
| ------ | ------- |
| `-a` | Use arch-style version formatting, replacing `-` with `.` |
| `-v` | Remove the `v` prefix from the beginning of a tag |

# Installation

While the script itself is just a shell script and requires no compilation, you can still build the docs to install.

```bash
make doc && sudo make install
```

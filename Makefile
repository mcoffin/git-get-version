.DEFAULT_GOAL := doc
SCDOC ?= scdoc
GZIP ?= gzip
PREFIX ?= /usr/local
DESTDIR ?= /.

doc_sources=$(shell find . -type f -name '*.scd')
doc_targets=$(doc_sources:.scd=.gz)

%.gz: %.scd
	$(SCDOC) < $^ | $(GZIP) > $@

doc: $(doc_targets)

clean:
	for f in $(doc_targets); do rm $$f || true; done

install: git-get-version $(doc_targets)
	install -D -m 755 -t "$(DESTDIR)/$(PREFIX)/bin" git-get-version
	$(foreach f,$(doc_targets),install -D -m 644 -t "$(DESTDIR)/$(PREFIX)/share/man/man1" $(f))

.PHONY: doc clean install
